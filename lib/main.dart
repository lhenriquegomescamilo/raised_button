import 'package:flutter/material.dart';

import 'custom-circle-avatar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(body: CircleButtonWidget()));
  }
}

class CircleButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(child: Text("j"), backgroundColor: Colors.purple),
          SizedBox(
            height: 50,
          ),
          CustomerCircleAvatar(size: 40, backgroundColor: Colors.red, text: "J")
        ],
      ),
    );
  }
}

//class GroupRaisedButton extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      width: double.infinity,
//      height: double.infinity,
//      child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
//          crossAxisAlignment: CrossAxisAlignment.center,
//          children: <Widget>[
//            RaisedButton(
//                color: Colors.white,
//                onPressed: () {
//                  print("Raised Button");
//                },
//                child: Text("Clique-me")),
//            RaisedButton.icon(
//                onPressed: () {},
//                icon: Icon(Icons.android),
//                label: Text("Clique aqui"))
//          ]),
//    );
//  }
//}
//class GroupRaisedButton extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      width: double.infinity,
//      height: double.infinity,
//      child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
//          crossAxisAlignment: CrossAxisAlignment.center,
//          children: <Widget>[
//            RaisedButton(
//                color: Colors.white,
//                onPressed: () {
//                  print("Raised Button");
//                },
//                child: Text("Clique-me")),
//            RaisedButton.icon(
//                onPressed: () {},
//                icon: Icon(Icons.android),
//                label: Text("Clique aqui"))
//          ]),
//    );
//  }
//}
