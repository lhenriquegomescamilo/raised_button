import 'package:flutter/material.dart';

class CustomerCircleAvatar extends StatelessWidget {
  final double size;
  final Widget child;
  final Color backgroundColor;
  final String text;

  const CustomerCircleAvatar(
      {Key key,
      this.size = 50,
      this.backgroundColor = Colors.red,
      this.child,
      this.text = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: this.backgroundColor,
            borderRadius: BorderRadiusDirectional.circular(this.size)),
        height: this.size,
        width: this.size,
        alignment: Alignment.center,
        child: Text(
          this.text,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14),
        ));
  }
}
